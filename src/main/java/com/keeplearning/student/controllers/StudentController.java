package com.keeplearning.student.controllers;

import com.keeplearning.student.entities.Student;
import com.keeplearning.student.requests.StudentRequest;
import com.keeplearning.student.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping
    public @ResponseBody
    List<Student> getAllStudent() {
        return studentService.getAllStudent();
    }

    @PostMapping
    public @ResponseBody void addNewStudent(@RequestBody StudentRequest studentRequest) {
        studentService.addNew(studentRequest);
    }

    @PutMapping("/{id}")
    public @ResponseBody void updateStudent(@PathVariable Long id, @RequestBody StudentRequest studentRequest) {
        studentService.update(id, studentRequest);
    }

    @DeleteMapping("/{id}")
    public @ResponseBody void deleteStudent(@PathVariable Long id) {
        studentService.delete(id);
    }

    @GetMapping("/{id}")
    public @ResponseBody Optional<Student> getStudentById(@PathVariable Long id) {
        return studentService.getStudentById(id);
    }
}
