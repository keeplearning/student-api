package com.keeplearning.student.services;

import com.keeplearning.student.entities.Student;
import com.keeplearning.student.repositories.StudentRepository;
import com.keeplearning.student.requests.StudentRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Transactional
    public void addNew(StudentRequest studentRequest) {
        Student student = new Student();
        student.setName(studentRequest.getName());
        student.setLastName(studentRequest.getLastName());
        student.setAge(studentRequest.getAge());
        student.setGender(studentRequest.getGender());
        student.setBirthday(studentRequest.getBirthday());
        studentRepository.save(student);
    }

    @Transactional
    public void update(Long id, StudentRequest studentRequest) {
        Optional<Student> student = studentRepository.findById(id);
        if (!student.isPresent())  {
            throw new RuntimeException("Student Not Found!");
        }
        student.get().setName(studentRequest.getName());
        student.get().setLastName(studentRequest.getLastName());
        student.get().setAge(studentRequest.getAge());
        student.get().setGender(studentRequest.getGender());
        student.get().setBirthday(studentRequest.getBirthday());
        studentRepository.save(student.get());
    }

    @Transactional
    public void delete(Long id) {
        Optional<Student> student = studentRepository.findById(id);
        if (!student.isPresent())  {
            throw new RuntimeException("Student Not Found!");
        }
        studentRepository.deleteById(id);
    }

    @Transactional
    public Optional<Student> getStudentById(Long id) {
        return studentRepository.findById(id);
    }

    @Transactional
    public List<Student> getAllStudent() {
        return studentRepository.findAll();
    }
}
