# Student-API

API Manage Student Data, driven on spring boot

### Get All Student
```
Method -> GET
Url -> localhost:8888/api/student
Header -> {
    Content-Type:application/json
    Accept:application/json
}
```
### Add New Student
```
Method -> POST
Url -> localhost:8888/api/student
Header -> {
    Content-Type:application/json
    Accept:application/json
}
Body -> {
    "name": "Angkarn",
    "lastName": "Janjuang",
    "age": 30,
    "gender": "Male",
    "birthday": "14/06/1988"
}
```
### Find Student By Id
```
Method -> GET
Url -> localhost:8888/api/student/{id}
Header -> {
    Content-Type:application/json
    Accept:application/json
}
```
### Update Student
```
Method -> PUT
Url -> localhost:8888/api/student/{id}
Header -> {
    Content-Type:application/json
    Accept:application/json
}
Body -> {
    "name": "Angkarn",
    "lastName": "Janjuang",
    "age": 30,
    "gender": "Male",
    "birthday": "14/06/1988"
}
```
### Delete Student
```
Method -> DELETE
Url -> localhost:8888/api/student/{id}
Header -> {
    Content-Type:application/json
    Accept:application/json
}
```
## How to use
```
run command: gradle bootRun